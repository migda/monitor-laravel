<?php

namespace Tests\Feature\Services;

use App\DTO\TransferStats\StatsObjectCollection;
use App\Jobs\CollectStatsForMonitor;
use App\Models\Monitor;
use App\Models\MonitorStat;
use App\Services\MonitorService;
use App\Services\TransferStatService;
use Illuminate\Http\Response;
use Tests\GuzzleTrait;
use Tests\TestCase;

class MonitorServiceTest extends TestCase
{
    /**
     * @var MonitorService
     */
    protected $monitorService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->monitorService = resolve(MonitorService::class);
    }

    /**
     * @test
     */
    public function it_can_store_multiple_new_monitors_in_database()
    {
        // Arrange
        $monitors = factory(Monitor::class, 4)->make();

        // Act
        $this->monitorService->storeMultiple($monitors->pluck('url')->toArray());

        // Test
        /**
         * @var Monitor $monitor
         */
        foreach ($monitors as $monitor) {
            $this->assertDatabaseHas('monitors', ['url' => $monitor->url]);
        }
    }

    /**
     * @test
     */
    public function it_dispatch_collect_stats_for_monitor_job_if_store_with_stats()
    {
        // Arrange
        /**
         * @var Monitor $monitor
         */
        $monitor = factory(Monitor::class)->make();

        // Act & Test
        $this->expectsJobs(CollectStatsForMonitor::class);
        $this->monitorService->store($monitor->url, true);
    }

    /**
     * @test
     */
    public function it_can_get_last_results_with_sum_of_total_time_from_all_redirects()
    {
        // TODO
        $this->assertTrue(true);
    }
}
