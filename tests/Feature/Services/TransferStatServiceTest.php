<?php

namespace Tests\Feature\Services;

use App\DTO\TransferStats\StatsObjectCollection;
use App\Services\TransferStatService;
use Illuminate\Http\Response;
use Tests\GuzzleTrait;
use Tests\TestCase;

class TransferStatServiceTest extends TestCase
{
    use GuzzleTrait;

    /**
     * @test
     */
    public function it_returns_stats_object_collection_after_successfully_request()
    {
        // Arrange
        $okResponse = $this->makeFakeResponse(Response::HTTP_OK);
        $transferStatService = new TransferStatService($this->createClientWithResponses([$okResponse]));

        // Act
        $test = $transferStatService->getTransferStatsForUrl("http://onet.pl");

        // Test
        $this->assertInstanceOf(StatsObjectCollection::class, $test);
    }
}
