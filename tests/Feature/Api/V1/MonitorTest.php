<?php

namespace Tests\Feature\Api\V1;

use App\Jobs\CollectStatsForMonitor;
use App\Models\Monitor;
use Tests\TestCase;

class MonitorTest extends TestCase
{
    /**
     * @test
     */
    public function it_can_store_multiple_monitors_in_database_if_none_exits_in_database_before()
    {
        // Arrange
        $monitors = factory(Monitor::class, 4)->make();

        // Act
        $response = $this->postJson(route('api.v1.monitors.store'), $monitors->pluck('url')->toArray());

        // Tests
        $response->assertOk();

        /**
         * @var Monitor $monitor
         */
        foreach ($monitors as $monitor) {
            $this->assertDatabaseHas('monitors', ['url' => $monitor->url]);
        }
    }

    /**
     * @test
     */
    public function it_returns_proper_json_structure_after_successfully_request()
    {
        // Arrange
        $monitors = factory(Monitor::class, 2)->make();

        // Act
        $response = $this->postJson(route('api.v1.monitors.store'), $monitors->pluck('url')->toArray());

        // Tests
        $response->assertJsonStructure([
            'code',
            'data',
            'messages',
        ]);
    }

    /**
     * @test
     */
    public function it_doesnt_add_stats_header_and_doesnt_dispatch_collect_job_if_there_is_no_stats_query_param_set_to_true()
    {
        // Arrange
        $monitors = factory(Monitor::class, 2)->make();

        // Act
        $response = $this->postJson(route('api.v1.monitors.store'), $monitors->pluck('url')->toArray());

        // Tests
        $response->assertHeaderMissing(config('monitor.stats_header'));
    }

    /**
     * @test
     */
    public function it_adds_stats_header_and_dispatches_collect_job_if_there_is_stats_query_param_set_to_true()
    {
        // Arrange
        $monitors = factory(Monitor::class, 2)->make();

        // Act & Tests
        $this->expectsJobs(CollectStatsForMonitor::class);

        $response = $this->postJson(route('api.v1.monitors.store', ['stats' => true]),
            $monitors->pluck('url')->toArray());

        $response->assertHeader(config('monitor.stats_header'));
    }
}
