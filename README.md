# Monitor

Url monitoring app.

## Installation

### Server Requirements

- PHP ^7.3|^8.0
- Mysql ^5.7
- Redis

### Installing project

#### Laravel

- `composer install`
- `cp .env.example .env` and set up .env
- `php artisan key:generate`
- `php artisan migrate`

#### Supervisor configuration

You can find supervisor configuration in laravel-workers.conf file in the main directory.

#### Cron

`* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1`

#### PHPUnit

- `cp .env.example .env.testing` and set up .env.testing
- `php artisan key:generate --env=testing`

and run `phpunit` command.

## Usage

You can find swagger docs file in `docs` directory.

#### Endpoints:

- POST /api/v1/monitors
- GET /api/v1/monitors/{url}

## TODO

- More tests.
- Handle guzzle timeout exception.
