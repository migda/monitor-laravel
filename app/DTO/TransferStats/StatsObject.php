<?php

namespace App\DTO\TransferStats;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * Add more fields if you need https://www.php.net/manual/ro/function.curl-getinfo.php
 *
 * Class StatsObject
 * @package App\DTO\TransferStats
 */
class StatsObject extends DataTransferObject
{
    /**
     * Total transaction time in seconds for last transfer
     *
     * @var float
     */
    public $total_time;

    /**
     * Last effective URL
     *
     * @var string
     */
    public $url;
}
