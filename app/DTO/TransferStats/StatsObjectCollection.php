<?php

namespace App\DTO\TransferStats;

use Spatie\DataTransferObject\DataTransferObjectCollection;

class StatsObjectCollection extends DataTransferObjectCollection
{
    /**
     * @return StatsObject
     */
    public function current(): StatsObject
    {
        return parent::current();
    }

    /**
     * @param array $data
     * @return static
     */
    public static function create(array $data): self
    {
        return new static(StatsObject::arrayOf($data));
    }
}
