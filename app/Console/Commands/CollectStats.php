<?php

namespace App\Console\Commands;

use App\Services\CollectStatService;
use Illuminate\Console\Command;

class CollectStats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monitor:collect-stats';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Collect stats for all monitors.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(CollectStatService $collectStatService)
    {
        $collectStatService->collectStatsForAllMonitors();
    }
}
