<?php

namespace App\Services;

use App\Jobs\CollectStatsForMonitor;
use App\Models\Monitor;
use GuzzleHttp\Exception\GuzzleException;

class CollectStatService
{
    /**
     * @var TransferStatService
     */
    protected $transferStatService;

    /**
     * MonitorService constructor.
     * @param TransferStatService $transferStatService
     */
    public function __construct(TransferStatService $transferStatService)
    {
        $this->transferStatService = $transferStatService;
    }

    /**
     * @return void
     */
    public function collectStatsForAllMonitors(): void
    {
        $monitors = Monitor::all();
        foreach ($monitors as $monitor) {
            CollectStatsForMonitor::dispatch($monitor->getKey())->onQueue($monitor->getQueueName());
        }
    }

    /**
     * @param Monitor $monitor
     * @return Monitor
     * @throws GuzzleException
     */
    public function collectStatsForMonitor(Monitor $monitor): Monitor
    {
        $transferStats = $this->transferStatService->getTransferStatsForUrl($monitor->url);
        $monitor->setTransferStats($transferStats);

        return $monitor;
    }
}
