<?php

namespace App\Services;

use App\DTO\TransferStats\StatsObjectCollection;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\TransferStats;

class TransferStatService
{
    /**
     * @var Client
     */
    protected $httpClient;

    /**
     * TransferStatService constructor.
     */
    public function __construct(Client $httpClient = null)
    {
        $this->httpClient = $httpClient ?? new Client();
    }

    /**
     * @param string $url
     * @return StatsObjectCollection
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getTransferStatsForUrl(string $url): StatsObjectCollection
    {
        $transferStats = [];
        $this->httpClient->request('GET', $url, $this->getRequestOptions($transferStats));

        return StatsObjectCollection::create($transferStats);
    }

    /**
     * @param array $transferStats
     * @return array
     */
    protected function getRequestOptions(array &$transferStats): array
    {
        return [
            RequestOptions::ON_STATS => function (TransferStats $stats) use (&$transferStats) {
                $transferStats[] = [
                    'total_time' => $stats->getTransferTime(),
                    'url' => (string)$stats->getEffectiveUri(),
                ];
            },
            RequestOptions::TIMEOUT => config('monitor.timeout'),
        ];
    }
}
