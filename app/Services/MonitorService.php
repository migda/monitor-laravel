<?php

namespace App\Services;

use App\Jobs\CollectStatsForMonitor;
use App\Models\Monitor;
use App\Models\MonitorStat;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class MonitorService
{
    /**
     * @var TransferStatService
     */
    protected $transferStatService;

    /**
     * MonitorService constructor.
     * @param TransferStatService $transferStatService
     */
    public function __construct(TransferStatService $transferStatService)
    {
        $this->transferStatService = $transferStatService;
    }

    /**
     * @param array $urls
     * @param bool $withSats
     * @return Collection
     */
    public function storeMultiple(array $urls, bool $withSats = false): Collection
    {
        $monitors = collect();
        foreach ($urls as $url) {
            $monitors->push($this->store($url, $withSats));
        }

        return $monitors;
    }

    /**
     * @param string $url
     * @param bool $withSats
     * @return Monitor
     */
    public function store(string $url, bool $withSats = false): Monitor
    {
        /**
         * @var Monitor $monitor
         */
        $monitor = Monitor::query()->where('url', $url)->first();

        // Do not create new entry in database if already exists
        if ($monitor) {
            return $monitor;
        }

        /**
         * @var Monitor $monitor
         */
        $monitor = Monitor::query()->create([
            'url' => $url
        ]);

        // Dispatch job if with stats
        if ($withSats) {
            CollectStatsForMonitor::dispatch($monitor->getKey())->onQueue($monitor->getQueueName());
        }

        return $monitor;
    }

    /**
     * @param Monitor $monitor
     * @param int $minutes
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getLastStats(Monitor $monitor, int $minutes = 10): \Illuminate\Database\Eloquent\Collection
    {
        return $monitor->stats()->withCount([
            'redirects AS total_time_sum' => function ($query) {
                $query->select(DB::raw("SUM(total_time) as total_time_sum"));
            }
        ])
            ->where('created_at', '>=', now()->subMinutes($minutes))
            ->get();
    }

    public function getFirstStatsForMonitors(Collection $monitors)
    {
        $stats = MonitorStat::query()
            ->whereIn('monitor_id', $monitors->pluck('id'))
            ->withCount([
                'redirects AS total_time_sum' => function ($query) {
                    $query->select(DB::raw("SUM(total_time) as total_time_sum"));
                }
            ])
            ->orderBy('id')
            ->get();

        // Map monitors with stats and redirects total time sum
        $monitorsMappedWithStats = $monitors->mapWithKeys(function (Monitor $monitor) use ($stats) {
            $stat = $stats->where('monitor_id', $monitor->getKey())->first();
            return [$monitor->url => $stat ? $stat->total_time_sum : null];
        });

        return $monitorsMappedWithStats->toArray();

    }
}
