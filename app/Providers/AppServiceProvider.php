<?php

namespace App\Providers;

use App\Helpers\ExecutionTimerHelper;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ExecutionTimerHelper::class, function ($app) {
            return new ExecutionTimerHelper();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Start measure execution time
        ExecutionTimerHelper::start();
    }
}
