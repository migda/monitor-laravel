<?php

namespace App\Helpers;

class ExecutionTimerHelper
{
    /**
     * @var float
     */
    private $startTime;

    /**
     * ExecutionTimerHelper constructor.
     */
    public function __construct()
    {
        $this->startTime = microtime(true);
    }

    /**
     * @return float
     */
    public function getStartTime(): float
    {
        return $this->startTime;
    }

    /**
     * @return $this
     */
    public function setStartTime(): ExecutionTimerHelper
    {
        $this->startTime = microtime(true);

        return $this;
    }

    /**
     * @return ExecutionTimerHelper
     */
    public static function start(): ExecutionTimerHelper
    {
        $executionTimer = resolve(ExecutionTimerHelper::class);

        return $executionTimer->setStartTime();
    }

    /**
     * @return float
     */
    public static function getExecutionTime(): float
    {
        $startTime = resolve(ExecutionTimerHelper::class)->getStartTime();
        $endTime = microtime(true);

        return $endTime - $startTime;
    }
}
