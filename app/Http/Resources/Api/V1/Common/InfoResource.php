<?php

namespace App\Http\Resources\Api\V1\Common;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Response;

class InfoResource extends JsonResource
{
    /**
     * @var int
     */
    protected $statusCode = Response::HTTP_OK;

    /**
     * @var array
     */
    protected $messages = [];

    /**
     * @var array
     */
    protected $errors = [];

    /**
     * @var array
     */
    protected $headers = [];

    /**
     * InfoResource constructor.
     * @param null $resource
     */
    public function __construct($resource = null)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'code' => $this->statusCode,
            'messages' => $this->messages,
            'data' => $this->resource,
        ];
    }

    /**
     * @param int $statusCode
     * @return InfoResource
     */
    public function setStatusCode(int $statusCode): self
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * @param string $key
     * @param string $value
     * @return $this
     */
    public function attachHeader(string $key, string $value): self
    {
        $this->headers[$key] = $value;

        return $this;
    }

    /**
     * @param array $messages
     * @return InfoResource
     */
    public function setMessages(array $messages): self
    {
        $this->messages = $messages;

        return $this;
    }

    /**
     * @param array $errors
     * @return InfoResource
     */
    public function setErrors(array $errors): self
    {
        $this->errors = $errors;

        return $this;
    }

    /**
     * Customize the outgoing response status code for the resource.
     *
     * @param $request
     * @param $response
     * @return void
     */
    public function withResponse($request, $response)
    {
        // Set status code to response
        $response->setStatusCode($this->statusCode);

        // Attach headers to response
        foreach ($this->headers as $key => $value) {
            $response->header($key, $value);
        }
    }
}
