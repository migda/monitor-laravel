<?php

namespace App\Http\Controllers\Api\V1\Monitor;

use App\Helpers\ExecutionTimerHelper;
use App\Models\Monitor;
use App\Http\Requests\Api\V1\Monitor\StoreRequest;
use App\Http\Resources\Api\V1\Common\InfoResource;
use App\Services\MonitorService;
use Illuminate\Support\Collection;

class MonitorController
{
    /**
     * Add urls to monitor
     *
     * @param StoreRequest $storeRequest
     * @param MonitorService $monitorService
     * @return InfoResource
     */
    public function store(StoreRequest $storeRequest, MonitorService $monitorService): InfoResource
    {
        // Prepare data
        $urls = $storeRequest->except(array_keys($storeRequest->query()));
        $withStats = filter_var($storeRequest->query('stats'), FILTER_VALIDATE_BOOLEAN);

        // Store all urls in database
        $monitors = $monitorService->storeMultiple($urls, $withStats);

        return $this->getInfoResource($monitors, $withStats, $monitorService);
    }

    /**
     * @param Monitor $monitor
     * @param MonitorService $monitorService
     * @return InfoResource
     */
    public function show(Monitor $monitor, MonitorService $monitorService): InfoResource
    {
        $stats = $monitorService->getLastStats($monitor);

        return (new InfoResource($stats->pluck('total_time_sum')));
    }

    /**
     * @param Collection $monitors
     * @param $withStats
     * @param MonitorService $monitorService
     * @return InfoResource
     */
    protected function getInfoResource(Collection $monitors, $withStats, MonitorService $monitorService): InfoResource
    {
        // Create resource
        $resource = (new InfoResource($monitors));

        // Check if should collect stats
        if ($withStats) {
            $countMonitors = count($monitors);
            // Pause until max execution time or processed all
            while (true) {
                // Run while loop until max execution time
                if (ExecutionTimerHelper::getExecutionTime() > config('monitor.max_execution_time_stats_request')) {
                    break;
                }

                // Or run while loop until all stats have been collected
                if (Monitor::query()->whereHas('stats')
                        ->whereIn('id', $monitors->pluck('id'))->count() === $countMonitors

                ) {
                    break;
                }

                // Check every 0.3 seconds
                sleep(0.3);
            }

            //  Attach header with already collected stats
            $resource->attachHeader(config('monitor.stats_header'),
                json_encode($monitorService->getFirstStatsForMonitors($monitors)));
        }

        return $resource;
    }
}
