<?php

namespace App\Http\Requests\Api\V1\Monitor;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Unique;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Urls inside request body
            '*' => [
                'string',
                'url',
                new Unique('monitors', 'url'),
            ],
            // Stats optional param
            'stats' => [
                'nullable',
            ],
        ];
    }
}
