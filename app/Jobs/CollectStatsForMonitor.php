<?php

namespace App\Jobs;

use App\Models\Monitor;
use App\Services\CollectStatService;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CollectStatsForMonitor implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Monitor
     */
    protected $monitorId;

    /**
     * Create a new job instance.
     *
     * @param int $monitorId
     */
    public function __construct(int $monitorId)
    {
        $this->monitorId = $monitorId;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws GuzzleException
     */
    public function handle(CollectStatService $collectStatService)
    {
        /**
         * @var Monitor $monitor
         */
        $monitor = Monitor::query()
            ->where('id', $this->monitorId)
            ->first();

        if ($monitor) {
            $collectStatService->collectStatsForMonitor($monitor);
        }
    }
}
