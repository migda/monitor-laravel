<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class MonitorStatRedirect
 * @package App\Models
 *
 * Database fields
 * @property int $monitor_stat_id
 * @property float $total_time
 * @property string $url
 *
 * Relations
 * @property MonitorStat $stat
 */
class MonitorStatRedirect extends Model
{
    /**
     * GLOBALS
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'monitor_stat_id',
        'total_time',
        'url',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    protected $casts = [
        'total_time' => 'float',
    ];

    /**
     * FUNCTIONS
     */

    /**
     * RELATIONS
     */

    /**
     * @return BelongsTo
     */
    public function stat(): BelongsTo
    {
        return $this->belongsTo(MonitorStat::class);
    }

    /**
     * SCOPES
     */

    /**
     * ACCESSORS
     */

    /**
     * MUTATORS
     */
}
