<?php

namespace App\Models;

use App\DTO\TransferStats\StatsObjectCollection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Monitor
 * @package App\App\Models
 *
 * Database fields
 * @property int $id
 * @property string $url
 *
 * Relations
 * @property null|Collection $stats
 */
class Monitor extends Model
{
    /**
     * GLOBALS
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    protected $casts = [
    ];

    /**
     * FUNCTIONS
     */
    public function getRouteKeyName()
    {
        return 'url';
    }

    /**
     * RELATIONS
     */

    /**
     * @return HasMany
     */
    public function stats(): HasMany
    {
        return $this->hasMany(MonitorStat::class);
    }

    /**
     * @return HasOne
     */
    public function lastStat(): HasOne
    {
        return $this->hasOne(MonitorStat::class)->orderByDesc('id')->limit(1);
    }

    /**
     * SCOPES
     */

    /**
     * ACCESSORS
     */

    /**
     * @return string
     */
    public function getQueueName(): string
    {
        $index = $this->getKey() % 4;

        return 'monitor-' . $index;
    }

    /**
     * MUTATORS
     */

    public function setTransferStats(StatsObjectCollection $transferStats): self
    {
        // Create entry in database with current monitor stat
        /**
         * @var MonitorStat $stat
         */
        $stat = $this->stats()->create();

        // Attach redirects to $monitorStat with data from $transferStats
        foreach ($transferStats as $transferStat) {
            $stat->redirects()->create($transferStat->toArray());
        }

        return $this;
    }
}
