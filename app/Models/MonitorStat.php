<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class MonitorStat
 * @package App\Models
 *
 * Database fields
 * @property int monitor_id
 *
 * Relations
 * @property Collection $redirects
 * @property Monitor $monitor
 */
class MonitorStat extends Model
{
    /**
     * GLOBALS
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'monitor_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    protected $casts = [
        'total_time_sum' => 'float',
    ];

    /**
     * FUNCTIONS
     */

    /**
     * RELATIONS
     */

    /**
     * @return HasMany
     */
    public function redirects(): HasMany
    {
        return $this->hasMany(MonitorStatRedirect::class);
    }

    /**
     * @return BelongsTo
     */
    public function monitor(): BelongsTo
    {
        return $this->belongsTo(Monitor::class);
    }

    /**
     * SCOPES
     */

    /**
     * ACCESSORS
     */

    /**
     * @return float|null
     */
    public function getTotalTime(): ?float
    {
        return $this->redirects->sum('total_time');
    }

    /**
     * MUTATORS
     */
}
