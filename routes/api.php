<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->name('api.v1.')->namespace('Api\V1')->group(function () {
    Route::prefix('monitors')->name('monitors.')->namespace('Monitor')->group(function () {
        Route::post('', 'MonitorController@store')->name('store');
        Route::get('{monitor}', 'MonitorController@show')->name('show')->where('monitor', '.*');
    });
});
