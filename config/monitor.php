<?php

return [
    'stats_header' => env('MONITOR_STATS_HEADER', 'X-Stats'), // Request timeout
    'timeout' => env('MONITOR_TIMEOUT', 30), // Request timeout
    'interval' => env('MONITOR_INTERVAL', 60), // Interval between collect stats for the monitor
    'max_execution_time_stats_request' => 2.5, // 2.5 seconds
];
