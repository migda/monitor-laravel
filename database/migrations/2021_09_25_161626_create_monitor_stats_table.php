<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMonitorStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monitor_stats', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('monitor_id');

            $table->timestamps();

            // Keys and indexes
            $table->foreign('monitor_id')
                ->references('id')
                ->on('monitors')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monitor_stats');
    }
}
