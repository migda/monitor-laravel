<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Monitor;
use App\Models\MonitorStat;
use Faker\Generator as Faker;

$factory->define(MonitorStat::class, function (Faker $faker) {
    return [
        'monitor_id' => function () {
            return factory(Monitor::class)->create()->getKey();
        },
    ];
});

