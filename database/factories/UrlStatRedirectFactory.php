<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\MonitorStat;
use App\Models\MonitorStatRedirect;
use Faker\Generator as Faker;

$factory->define(MonitorStatRedirect::class, function (Faker $faker) {
    return [
        'monitor_id' => function () {
            return factory(MonitorStat::class)->create()->getKey();
        },
    ];
});
